package coelho.paulo.atividade.Buffer;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class Buffer {

    public static FloatBuffer CriaBuffer(float[] array) {
        //Bytes de alocação na memória
        ByteBuffer vrByteBuffer = ByteBuffer.allocateDirect(array.length * 4);
        vrByteBuffer.order(ByteOrder.nativeOrder());

        //Crie um FloatBuffer
        FloatBuffer vrFloatBuffer = vrByteBuffer.asFloatBuffer();
        vrFloatBuffer.clear();

        //insere uma matriz java no buffer de flutuação
        vrFloatBuffer.put(array);
        //resetar floatBuffer atributos
        vrFloatBuffer.flip();

        return vrFloatBuffer;
    }
}