package coelho.paulo.atividade.Tangram.formatos;


import coelho.paulo.atividade.Tangram.Geometria;

public class Quadrado extends Geometria {

    public Quadrado(float largura, float altura) {
        float[] coordenadas = {
                -largura/2, -altura/2,
                largura/2, -altura/2,
                -largura/2, altura/2,
                largura/2, altura/2

        };


        setQtdArray(coordenadas.length);
        CriaBuffer(coordenadas);

    }
}