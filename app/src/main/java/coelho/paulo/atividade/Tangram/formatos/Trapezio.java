package coelho.paulo.atividade.Tangram.formatos;

import coelho.paulo.atividade.Tangram.Geometria;

public class Trapezio extends Geometria {
    public Trapezio(float largura, float altura) {
        float[] coordenadas = {

                -largura/2, -altura/2,
                largura/6, -altura/2,
                -largura/6, altura/2,
                largura/2, altura/2

        };
        setQtdArray(coordenadas.length);

        CriaBuffer(coordenadas);
    }

}