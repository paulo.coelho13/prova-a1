package coelho.paulo.atividade.Tangram.formatos;

import coelho.paulo.atividade.Tangram.Geometria;

public class Triangulo extends Geometria
{
    public Triangulo(float largura, float altura) {
        float[] coordenadas = {
                -largura/2, -altura/2,
                largura/2, -altura/2,
                0, altura/2

        };

        setQtdArray(coordenadas.length);
        CriaBuffer(coordenadas);

    }
}