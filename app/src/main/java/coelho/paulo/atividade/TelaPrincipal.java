package coelho.paulo.atividade;

import android.os.Bundle;
import android.opengl.GLSurfaceView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import coelho.paulo.atividade.Tangram.formatos.Quadrado;
import coelho.paulo.atividade.Tangram.formatos.Trapezio;
import coelho.paulo.atividade.Tangram.formatos.Triangulo;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;


class Renderizador implements GLSurfaceView.Renderer {

    int iFPS;
    long tempoInicial = 0;
    long tempoAtual = 0;
    float PosX = 000, PosY = 000;
    float PosLargura, PosAltura;
    int dir = 1, dir2 = 1;
    float angulo = 1;
    int lado = 200, H = 600;
    Triangulo triangulo0, triangulo1, triangulo2, triangulo3, triangulo4;
    Quadrado quadrado = null;
    Trapezio trapezio = null;

    @Override
    //será chamado quando o aplicativo for criado, 1 vez só
    public void onSurfaceCreated(GL10 vrOpengl, EGLConfig eglConfig) {

    }

    @Override
    //Vai ser chamada quando a superficie mudar
    public void onSurfaceChanged(GL10 vrOpenGL, int largura, int altura) {


        //configura a cor que sera utilizada para limpar o fundo da tela
        vrOpenGL.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        //Configura a area de visualização utilizada na tela do aparelho
        vrOpenGL.glViewport(0, 0, largura, altura);

        float unidade = largura / 10;
        float quadradoMedio = (float) Math.sqrt(Math.pow(unidade, 2) + (Math.pow(unidade, 2)));
        float trianguloMedio = (float) Math.sqrt(Math.pow(unidade * 2, 2) + (Math.pow(unidade * 2, 2)));

        triangulo0 = new Triangulo(unidade * 4, unidade * 2);
        triangulo1 = new Triangulo(unidade * 4, unidade * 2);
        triangulo2 = new Triangulo(unidade * 2, unidade);
        triangulo3 = new Triangulo(unidade * 2, unidade);
        triangulo4 = new Triangulo(trianguloMedio, trianguloMedio / 2);
        quadrado = new Quadrado(quadradoMedio, quadradoMedio);
        trapezio = new Trapezio(unidade * 3, unidade);


        triangulo0.setMover(unidade*5, unidade*5);
        float cor[] = {1.0f, 0.0f, 0.0f};//TRINAGULO VERMELHO
        triangulo0.setCor(cor);
        triangulo0.setAngulo(225);


        triangulo1.setMover(unidade*6.7f, unidade*6.2f);
        float cor2[] = {0f, 1.0f, 0f}; //TRIANGULO VERDE
        triangulo1.setCor(cor2);
        triangulo1.setAngulo(270);


        triangulo2.setMover(unidade*1.85f, unidade*2.55f);
        float cor3[] = {1.0f, 1.0f, 0.0f};
        triangulo2.setCor(cor3);
        float escala2[] = {0.15f, 0.15f, 0.15f};

        triangulo3.setMover(unidade*7.2f,unidade*9.2f);
        float cor4[] = {0f, 0f, 0f}; //triangulo azul
        triangulo3.setCor(cor4);
        triangulo2.setAngulo(45);


        triangulo4.setMover(unidade*6.7f, unidade*8.2f);
        // float escala3[]= {0.75f,0.75f,0.75f};
        float cor5[] = {1.0f, 0.0f, 0.0f}; //triangulo vermelho
        triangulo4.setCor(cor5);
        triangulo4.setAngulo(-45);


        quadrado.setMover(unidade*5, unidade*3.6f);
        float corQuadrado[] = {0.3f, 0.17f, 0}; //quadrado marrom
        quadrado.setCor(corQuadrado);


        trapezio.setMover(unidade*2.9f, unidade*3.6f);
        float corTrapezio[] = {0, 0, 1.0f}; //trapezio azul
        trapezio.setCor(corTrapezio);
        trapezio.setAngulo(45);
        trapezio.setInvert();

        vrOpenGL.glMatrixMode(GL10.GL_PROJECTION);
        vrOpenGL.glLoadIdentity();  // carrega a matriz identidade para tirar o lixo da memoria
        vrOpenGL.glOrthof(0, largura, 0, altura, 1, -1);
        vrOpenGL.glMatrixMode(GL10.GL_MODELVIEW);
        vrOpenGL.glLoadIdentity();
        vrOpenGL.glEnableClientState(GL10.GL_VERTEX_ARRAY);

    }

    @Override

    public void onDrawFrame(GL10 vrOpengl) {
        vrOpengl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        triangulo0.desenha(vrOpengl);
        triangulo1.desenha(vrOpengl);
        triangulo2.desenha(vrOpengl);
        triangulo3.desenha(vrOpengl);
        triangulo4.desenha(vrOpengl);

        quadrado.desenha(vrOpengl);
        trapezio.desenha(vrOpengl);


    }

}


public class TelaPrincipal extends AppCompatActivity {
    //Cria uma variavel de referencia para a OpenGL
    GLSurfaceView superficieDesenho = null;
    Renderizador render = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Valida a variavel de referencia com uma instancia da superficie
        superficieDesenho = new GLSurfaceView(this);
        render = new Renderizador();
        //Ligando classe
        superficieDesenho.setRenderer(render);
        //Configura a tela do aparelho para mostrar a sup. de desenho
        setContentView(superficieDesenho);
        //IMPRIME UMA MENSAGEM NO LOG COM A TAG FPS E O TEXTO DO 2 PARAMETRO
        Log.i("FPS", "Alguma coisa");

    }
}
